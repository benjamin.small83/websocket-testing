from flask_socketio import SocketIO

socketio = SocketIO(message_queue = "amqp://guest:guest@rabbitmq.sys//")

socketio.emit("default", {"message": "message from external producer"})